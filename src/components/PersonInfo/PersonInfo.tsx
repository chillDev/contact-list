import { FC, memo } from 'react';
import { divideNameAndSurnameOnArrays } from 'src/helpers';

import { IPersonInfoComponent } from './types';

import {
  BusinessCardWrapper,
  EmailAddress,
  FirstNameLastName,
  InitialLetters,
  JobTitle,
  NameAndJobTitleWrapper,
  PersonInfoWrapper,
} from './styles';

export const PersonInfo: FC<IPersonInfoComponent> = memo(
  ({ data, isSelectedInfoList, selectPersonInfo }) => {
    const { id, firstNameLastName, jobTitle, emailAddress } = data;

    const nameAndSurnameInArray =
      divideNameAndSurnameOnArrays(firstNameLastName);
    const firstLetterName = firstNameLastName[0];
    const firstLetterLastName = nameAndSurnameInArray[1][0];

    return (
      <PersonInfoWrapper
        onClick={() => selectPersonInfo(isSelectedInfoList, id, data)}
        isSelectedInfoList={isSelectedInfoList}
      >
        <BusinessCardWrapper>
          <InitialLetters>
            {firstLetterName}
            {firstLetterLastName}
          </InitialLetters>
          <NameAndJobTitleWrapper>
            <FirstNameLastName>{firstNameLastName}</FirstNameLastName>
            <JobTitle>{jobTitle}</JobTitle>
          </NameAndJobTitleWrapper>
        </BusinessCardWrapper>
        <EmailAddress>{emailAddress}</EmailAddress>
      </PersonInfoWrapper>
    );
  }
);
