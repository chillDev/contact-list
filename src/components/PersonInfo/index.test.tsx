import { render, screen } from '@testing-library/react';
import { IPersonInfo } from 'src/types';
import PersonInfo from './';

const mockData: IPersonInfo = {
  firstNameLastName: 'Harry Potter',
  jobTitle: 'Wizard',
  emailAddress: 'hp@hogwart.com',
  id: '10',
};

const mockCallback = jest.fn();

describe('Test Component PersonInfo', () => {
  it('should contains name and surname', () => {
    render(
      <PersonInfo
        data={mockData}
        key={mockData.id}
        isSelectedInfoList={false}
        selectPersonInfo={mockCallback}
      />
    );
    expect(screen.getByText(mockData.firstNameLastName)).toBeInTheDocument();
  });
});
