import { IPersonInfo } from 'src/types';

export interface IPersonInfoComponent {
  data: IPersonInfo;
  isSelectedInfoList: boolean;
  selectPersonInfo: (
    isSelectedInfoList: boolean,
    id: string,
    PersonInfoData: IPersonInfo
  ) => void;
}

export interface ISelectPersonInfo {
  isSelectedInfoList: boolean;
  id: string;
  PersonInfoData: IPersonInfo;
}

export interface StyledCWrapperProps {
  isSelectedInfoList?: boolean;
}
