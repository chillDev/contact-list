import styled from 'styled-components';
import { StyledCWrapperProps } from './types';

export const PersonInfoWrapper = styled.div<StyledCWrapperProps>`
  display: flex;
  height: 80px;
  width: 280px;
  justify-content: space-between;
  flex-direction: column;
  padding: 20px;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
  margin: 10px 0;
  background: #fff;
  cursor: pointer;
  margin: 15px;
  &:hover {
    padding: 20px;
    border: 2px solid #e74c3c;
  }
  border: ${(p) =>
    p.isSelectedInfoList ? '2px solid #e74c3c' : '2px solid #FFF'};
`;
export const BusinessCardWrapper = styled.div`
  display: flex;
  justify-content: start;
  align-items: start;
`;

export const InitialLetters = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 30px;
  min-height: 30px;
  border-radius: 50%;
  border: 1px solid #000;
  font-size: 14px;
  margin: 0 8px 0 0;
`;

export const NameAndJobTitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
  overflow: hidden;
  white-space: nowrap;
`;

export const FirstNameLastName = styled.div`
  color: #333333;
  font-size: 16px;
  font-weight: 700;
  max-height: 20px;
`;

export const JobTitle = styled.div`
  color: #e74c3c;
  font-size: 14px;
  font-weight: 400;
`;

export const EmailAddress = styled.div`
  color: #666666;
  font-size: 12px;
  line-height: 1.8em;
`;
