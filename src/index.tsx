import App from './App';
import ReactDOM from 'react-dom/client';

import GlobalStyle from './globalStyle';

const root = ReactDOM.createRoot(document.getElementById('root') as Element);
root.render(
  <>
    <GlobalStyle />
    <App />
  </>
);
