export type IPersonInfo = {
  firstNameLastName: string;
  jobTitle: string;
  emailAddress: string;
  id: string;
};

export type IPersonInfoList = IPersonInfo[];
