import { FC, useEffect, useState, useCallback } from 'react';
import apiData from '../api';

import { removeObjectWithId } from 'src/helpers';
import { IPersonInfoList, IPersonInfo } from '../types';

import PersonInfo from 'src/components/PersonInfo';

import {
  AppWrapper,
  ButtonExpandList,
  ErrorMessage,
  SelectedContacts,
} from './styles';

export const App: FC = () => {
  const [data, setData] = useState<IPersonInfoList | []>([]);
  const [selectedData, setSelectedData] = useState<IPersonInfoList | []>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [errorMsg, setErrorMsg] = useState<string>('');

  const fetchList = async () => {
    try {
      const response: IPersonInfoList = await apiData();
      setData((data) => [...data, ...response]);
      setErrorMsg('');
    } catch (error) {
      console.error(error);
      setErrorMsg('Error while loading data. Press button to try again.');
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchList();
  }, []);

  const onAddMorePersonalInfoData = () => {
    setIsLoading(true);
    fetchList();
  };

  const selectPersonInfo = useCallback(
    (isSelectedInfoList: boolean, id: string, PersonInfoData: IPersonInfo) => {
      if (!isSelectedInfoList) {
        setSelectedData((selectedData) => [...selectedData, PersonInfoData]);
        setData((data) => [...removeObjectWithId(data, id)]);
      } else {
        setData((data) => [...data, PersonInfoData]);
        setSelectedData((selectedData) => [
          ...removeObjectWithId(selectedData, id),
        ]);
      }
    },
    []
  );

  const isErrorMsg = errorMsg.length > 0;
  const isListLoading =
    data.length === 0 && !isErrorMsg && selectedData.length === 0;

  return (
    <AppWrapper>
      <SelectedContacts>
        Selected contacts: {selectedData.length}
      </SelectedContacts>
      <div>
        {isListLoading ? (
          <div>List is loading...</div>
        ) : (
          <>
            {selectedData.map((personInfo) => (
              <PersonInfo
                key={personInfo.id}
                data={personInfo}
                selectPersonInfo={selectPersonInfo}
                isSelectedInfoList={true}
              />
            ))}
            {data.map((personInfo) => (
              <PersonInfo
                key={personInfo.id}
                data={personInfo}
                selectPersonInfo={selectPersonInfo}
                isSelectedInfoList={false}
              />
            ))}
          </>
        )}
      </div>
      {isErrorMsg ? <ErrorMessage>{errorMsg}</ErrorMessage> : null}
      <ButtonExpandList
        isLoading={isLoading}
        disabled={isLoading}
        onClick={onAddMorePersonalInfoData}
      >
        {isLoading ? 'Loading' : 'Add more'}
      </ButtonExpandList>
    </AppWrapper>
  );
};
