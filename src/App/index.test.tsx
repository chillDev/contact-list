import { fireEvent, render, screen } from '@testing-library/react';
import App from './';

describe('Test Component App', () => {
  it('should render info about loading list', () => {
    render(<App />);
    expect(screen.getByText('List is Loading...')).toBeInTheDocument();
  });
  it('should render selectred contacts with 0 value', () => {
    render(<App />);
    expect(screen.getByText('Selected contacts: 0')).toBeInTheDocument();
  });

  test('add more button being clicked change text to loading', () => {
    render(<App />);
    const addMoreButton = screen.getByRole('button');
    fireEvent.click(addMoreButton);
    expect(addMoreButton).toHaveTextContent('Loading');
  });

  it('add more button being clicked and disabled', () => {
    render(<App />);
    const addMoreButton = screen.getByRole('button');
    fireEvent.click(addMoreButton);
    expect(addMoreButton).toBeDisabled();
  });
});
