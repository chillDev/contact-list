import styled from 'styled-components';
import { StyledButtonExpandListProps } from './types';

export const AppWrapper = styled.div`
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const SelectedContacts = styled.h2`
  margin: 25px 0 10px 0;
`;

export const ErrorMessage = styled.div`
  color: #ff0000;
  font-weight: bold;
`;

export const ButtonExpandList = styled.button<StyledButtonExpandListProps>`
  margin: 20px 0 35px 0;
  padding: 15px;
  border: 2px solid #111;
  cursor: pointer;
  color: ${(p) => (p.isLoading ? '#e74c3c' : '#000')};
`;
