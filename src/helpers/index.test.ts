import { removeObjectWithId, divideNameAndSurnameOnArrays } from './index';

const mockTestData = [
  {
    id: '85',
    jobTitle: 'Stockbroker',
    emailAddress: 'Alexa_Ogilvy1833@gompie.com',
    firstNameLastName: 'Alexa Ogilvy',
  },
  {
    id: '86',
    jobTitle: 'Cook',
    emailAddress: 'Morgan_Cooper7675@deavo.com',
    firstNameLastName: 'Morgan Cooper',
  },
  {
    id: '87',
    jobTitle: 'Restaurant Manager',
    emailAddress: 'Marvin_Watson5165@brety.org',
    firstNameLastName: 'Marvin Watson',
  },
  {
    id: '88',
    jobTitle: 'Software Engineer',
    emailAddress: 'Christy_Alcroft1958@infotech44.tech',
    firstNameLastName: 'Christy Alcroft',
  },
];

describe('test function removeObjectWithId', () => {
  test('returns correctly formatted array of objects without object with provided id', () => {
    expect(removeObjectWithId(mockTestData, '86')).toMatchObject([
      {
        id: '85',
        jobTitle: 'Stockbroker',
        emailAddress: 'Alexa_Ogilvy1833@gompie.com',
        firstNameLastName: 'Alexa Ogilvy',
      },
      {
        id: '87',
        jobTitle: 'Restaurant Manager',
        emailAddress: 'Marvin_Watson5165@brety.org',
        firstNameLastName: 'Marvin Watson',
      },
      {
        id: '88',
        jobTitle: 'Software Engineer',
        emailAddress: 'Christy_Alcroft1958@infotech44.tech',
        firstNameLastName: 'Christy Alcroft',
      },
    ]);
  });
  it('shouldnt contain provided array of objects with provided id', () => {
    expect(removeObjectWithId(mockTestData, '88')).not.toBe([
      {
        id: '85',
        jobTitle: 'Stockbroker',
        emailAddress: 'Alexa_Ogilvy1833@gompie.com',
        firstNameLastName: 'Alexa Ogilvy',
      },
      {
        id: '87',
        jobTitle: 'Restaurant Manager',
        emailAddress: 'Marvin_Watson5165@brety.org',
        firstNameLastName: 'Marvin Watson',
      },
      {
        id: '88',
        jobTitle: 'Software Engineer',
        emailAddress: 'Christy_Alcroft1958@infotech44.tech',
        firstNameLastName: 'Christy Alcroft',
      },
    ]);
  });
});

describe('test function divideNameAndSurnameOnArrays', () => {
  it('should divide string to two elements of array', () => {
    expect(divideNameAndSurnameOnArrays('John Snow')).toEqual(['John', 'Snow']);
  });
  it('shouldnt be equal these two elements of array', () => {
    expect(divideNameAndSurnameOnArrays('John Snow')).not.toEqual([
      'John',
      'Targaryen',
    ]);
  });
  it('should divide string for string with full name and empty string', () => {
    expect(divideNameAndSurnameOnArrays('JohnSnow')).toEqual(['JohnSnow', '']);
  });
});
