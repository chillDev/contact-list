import { IPersonInfoList } from 'src/types';

export const removeObjectWithId = (arr: IPersonInfoList, id: string) => {
  const objWithIdIndex = arr.findIndex((obj) => obj.id === id);
  if (objWithIdIndex > -1) {
    arr.splice(objWithIdIndex, 1);
  }
  return arr;
};

export const divideNameAndSurnameOnArrays = (str: string) =>
  str.match(/^\s*(\S+)\s*(.*?)\s*$/)!.slice(1);
